using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introdu primul numar:");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Introdu al doilea numar");
            int b = int.Parse(Console.ReadLine());

            int r = SumaMul(a, b);

            Console.WriteLine(r);
            Console.ReadKey();

        }

        public static int SumaMul(int n, int m)
        {
            int i = 1;
            int suma = 0;
            while (n * i < m)
            {
                
                suma = suma + n * i;
                i++;
            }
            return suma;
        }
    }
}