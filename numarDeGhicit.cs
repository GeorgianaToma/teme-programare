using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Aici am creat numarul generat automat de la 0 la 100.

            Random rand = new Random();
            int numarGenerat = rand.Next(100);

            Console.WriteLine(numarGenerat);

            Console.WriteLine("Incearca sa ghicesti numarul: ");

            //Aici am initializat numarul pe care vom incerca sa il ghicim, cat si numarul de incercari, ce este egal cu 0 la inceput.

            int numarDeGhicit = int.Parse(Console.ReadLine());
            int numarIncercari = 1;


            do
            {   
                if (numarDeGhicit == numarGenerat)//Daca am ghicit numarul din prima incercare.
                {
                    Console.WriteLine("BRAVO! Ai ghicit numarul din prima!");
                }
                if (numarDeGhicit > numarGenerat + 50 || numarDeGhicit < numarGenerat - 50)   
                {
                    Console.WriteLine("Foarte rece!");
                    numarIncercari++;
                    numarDeGhicit = int.Parse(Console.ReadLine());
                }
                if (numarDeGhicit > numarGenerat + 20 && numarDeGhicit <= numarGenerat + 50 || numarDeGhicit < numarGenerat - 20 && numarDeGhicit >= numarGenerat - 50)
                {
                    Console.WriteLine("Rece!");
                    numarIncercari++;
                    numarDeGhicit = int.Parse(Console.ReadLine());
                }
                if (numarDeGhicit > numarGenerat + 10 && numarDeGhicit <= numarGenerat + 20 || numarDeGhicit < numarGenerat - 10 && numarDeGhicit >= numarGenerat - 20)
                {
                    Console.WriteLine("Caldut!");
                    numarIncercari++;
                    numarDeGhicit = int.Parse(Console.ReadLine());
                }
                if (numarDeGhicit > numarGenerat + 5 && numarDeGhicit <= numarGenerat + 10 || numarDeGhicit < numarGenerat - 5 && numarDeGhicit >= numarGenerat - 10)
                {
                    Console.WriteLine("Cald!");
                    numarIncercari++;
                    numarDeGhicit = int.Parse(Console.ReadLine());
                }
                if (numarDeGhicit > numarGenerat + 3 && numarDeGhicit <= numarGenerat + 5 || numarDeGhicit < numarGenerat - 3 && numarDeGhicit >= numarGenerat - 5)
                {
                    Console.WriteLine("Fierbinte!");
                    numarIncercari++;
                    numarDeGhicit = int.Parse(Console.ReadLine());
                }
                if (numarDeGhicit > numarGenerat && numarDeGhicit <= numarGenerat + 3 || numarDeGhicit < numarGenerat && numarDeGhicit >= numarGenerat - 3)
                {
                    Console.WriteLine("Foarte fierbinte!");
                    numarIncercari++;
                    numarDeGhicit = int.Parse(Console.ReadLine());
                }
            } while (numarDeGhicit != numarGenerat);
           

            Console.WriteLine("Numarul ce trebuia ghicit era " + numarGenerat + " si ai reusit din " + numarIncercari + " incercari.");
            Console.ReadKey();

        }
    }